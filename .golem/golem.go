package main

import (
	"os"
	"path"
	"strings"

	"gitlab.com/ottersoft/golem"
)

func main() {
	// Define build tasks. Each task has a name and a callback function. The
	// callback function receives a Context object which contains task
	// information and utility funcions.

	golem.Task("restore", func(c golem.Context) {
		c.GoUpdate()
		c.GoTidy()
	})

	golem.Task("build", func(c golem.Context) {
		c.RunTask("restore")
		c.RunTask("test")
		out := "out/" + os.Getenv("GOOS") + "/" + os.Getenv("GOARCH")
		c.EnsureDir(out)
		c.Exec("go", "build", "-o="+out, "./...")

		c.Visit("app/*.yaml", func(f string) {
			if !strings.HasSuffix(f, "/config.local.yaml") {
				c.Copy(f, out+"/"+path.Base(f))
			}
		})
	})

	golem.Task("build-docker", func(c golem.Context) {
		os.Setenv("GOOS", "linux")
		os.Setenv("GOARCH", "amd64")
		c.RunTask("build")
		c.Exec("docker", "build", "-t", "otterauth", ".")
	})

	golem.Task("run", func(c golem.Context) {
		c.RunTask("build")
		c.Command("./app").Dir("out/" + os.Getenv("GOOS") + "/" + os.Getenv("GOARCH")).Run()
	})

	golem.Task("run-docker", func(c golem.Context) {
		c.RunTask("build-docker")
		c.Exec("docker", "run", "-itp", "8080:8080", "otterauth")
	})

	golem.Task("cover", func(c golem.Context) {
		c.RunTask("test")
		c.Exec("go", "tool", "cover", "-html=cp.out")
	})

	golem.Task("test", func(c golem.Context) {
		c.Exec("go", "test", "-coverprofile", "cp.out", "./...")
		c.Exec("go", "tool", "cover", "-func=cp.out")
	})

	golem.Task("clean", func(c golem.Context) {
		c.Delete("**/out/")
		c.Delete("**/*.out")
	})

	// The Run() function executes the command indicated by the first argument
	// passed to Golem on the command line. This method will ALWAYS exit, so it
	// should be the very last call in main().
	golem.Run()
}
