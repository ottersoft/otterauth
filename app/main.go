package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	var address = fmt.Sprintf("%s:%d", Config.Host, Config.Port)
	log.Printf("Server starting on %s", address)
	log.Fatal(http.ListenAndServe(address, Router))
}
