package main

import (
	"os"

	r "gitlab.com/ottersoft/goapi/router"
	"gitlab.com/ottersoft/goapi/rule"
	"gitlab.com/ottersoft/otterauth/app/resources"
)

// Router is the main OtterAuth router.
var Router = r.NewRouter(
	rule.SetDebugLogWriter(os.Stderr),
	rule.AddRequestID(),
	rule.AddLogPrefix(func(req *r.Request, b *rule.LogPrefixBuilder) {
		b.Add(req.Meta(rule.KeyRequestIDs)...)
	}),
	rule.AddRequestLogging(),
	rule.AddCompression(),

	resources.Login(),
	resources.Logout(),
	resources.OpenID(),
)
