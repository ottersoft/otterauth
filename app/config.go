package main

import (
	"log"
	"os"

	c "gitlab.com/ottersoft/goapi/config"
)

// Config for OtterAuth.
var Config = struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}{
	Host: "127.0.0.1",
	Port: 8080,
}

func init() {
	// Load configuration files.
	for _, f := range c.Load(os.Getenv("ENVIRONMENT"), &Config) {
		if f.Error != nil {
			log.Printf(`Failed reading config file "%s": %v`, f.Filename, f.Error)
		} else {
			log.Printf(`Loaded config file "%s"`, f.Filename)
		}
	}

	c.LoadEnvironment(&Config, "Host=HOST", "Port=PORT")
}
