package resources

import (
	r "gitlab.com/ottersoft/goapi/router"
)

// Login returns a login resource rule.
func Login() r.Rule {
	return r.Resource("/login",
		r.Route(r.Get(r.Path("/")), func(res r.Response, req *r.Request) {
			res.WriteJSON(map[string]interface{}{})
		}),
	)
}
