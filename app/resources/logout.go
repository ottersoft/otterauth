package resources

import (
	r "gitlab.com/ottersoft/goapi/router"
)

// Logout returns a logout resource rule.
func Logout() r.Rule {
	return r.Resource("/logout",
		r.Route(r.Get(r.Path("/", "/:tenant")), func(res r.Response, req *r.Request) {
			res.WriteJSON(map[string]interface{}{})
		}),
	)
}
