package resources

import (
	r "gitlab.com/ottersoft/goapi/router"
)

// OpenID returns an .well-known resource rule.
func OpenID() r.Rule {
	return r.Resource("/.well-known",
		r.Route(r.Get(r.Path("/openid-configuration")), func(res r.Response, req *r.Request) {
			res.WriteJSON(map[string]interface{}{})
		}),
	)
}
