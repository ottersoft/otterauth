FROM golang:alpine
WORKDIR app
COPY out/linux/amd64 ./
ENV HOST "0.0.0.0"
CMD ./app